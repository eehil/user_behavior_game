# User Behavior Prediction Game

This is a game that models users behavior and makes the game
incrementally more difficult as the levels are passed. The idea of the game is simple for the player:
Hit the Click button until you win. However, the Click button changes place randomly and as the
levels progress, the number of buttons increases. This makes the game more difficult.