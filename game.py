import tkinter as tk
import random
import math
import time

SCORE = 0
LEVEL = 1
RANDOM_BUTTONS = []
POSSIBLE_BUTTONS = ["Save", "Help", "Find", "About","Print",
                    "Go","Save-as","Open-URL", "Share", "Close","Hide","Content"]
START = time.time()
avg_error = 0
last_expection = 0

def restart(button, text):
    global SCORE, LEVEL
    SCORE = 0
    LEVEL = 1
    button.place_forget()
    text.place_forget()
    move()

def fitts_law(distance):
    a = 0.2
    b = 0.2
    return round(a + b * math.log(distance + 1), 1)

def hicks_law(n):
    b = 0.4
    return round(b * math.log(n + 1), 1)

def move():
    global SCORE, LEVEL, START, avg_error
    SCORE += 1

    score_text.config(text="Score: {:d}".format(SCORE))

    level_text.config(text="Level: {:d}".format(LEVEL))

    if SCORE % 5 == 0:
        LEVEL += 1
    old_x = click_b.winfo_x()
    old_y = click_b.winfo_y()

    new_x = random.randint(0, 700)
    new_y = random.randint(30, 700)
    
    distance = math.sqrt((new_x - old_x)**2 + (new_y - old_y)**2)
    
    click_b.place(x=new_x, y=new_y)
    for n in range(100):
        T = fitts_law(distance) + hicks_law(n)
        if T > 0.1 * LEVEL + 1.3:
            print("Buttons: ", n + 1)
            random_buttons(n)
            break
    print("Expected time:", T)

    time_text.config(text="Expected selection time: {:.2f} sec".format(T))

    elapsed_time = time.time() - START
    START = time.time()
    elapsed_text.config(text="Previous selection time: {:.2f} sec".format(elapsed_time))

    avg_error = (avg_error * (SCORE - 1)  + abs(last_expection - elapsed_time) )/ SCORE

    print("Error:", avg_error)
    click_b.lift()
    if LEVEL > 9:
        win_txt = tk.Label(root, text="YOU WON THE GAME!")
        win_txt. place(x=250, y=350)
        win_txt.config(font=("Courier", 30))
        win_txt.lift()
        restart_b = tk.Button(root, text='Restart', command= lambda: restart(restart_b, win_txt))
        restart_b.place(x=350, y=500)
        restart_b.config(height=4, width=8)
        restart_b.lift()

        error_txt = tk.Label(root, text="Average estimation error: {:.1f} sec".format(avg_error))
        error_txt. place(x=200, y=400)
        error_txt.config(font=("Courier", 20))
        error_txt.lift()
    
    print("Score:", SCORE, "Level:", LEVEL)

def random_buttons(n):
    global RANDOM_BUTTONS
    for b in RANDOM_BUTTONS:
        RANDOM_BUTTONS.remove(b)
        b.place_forget()

    for i in range(n):
        text = random.sample(POSSIBLE_BUTTONS, 1)
        b  = tk.Button(root, text=text)
        b.place(x=random.randint(0, 700), y=random.randint(30, 700))
        b.config(height=4, width=8)
        RANDOM_BUTTONS.append(b)
        
root = tk.Tk()
root.geometry("800x800") #Width x Height
click_b = tk.Button(root, text="Click", command=move)
click_b.config(height=4, width=8)
click_b.place(x=0, y=30)

score_text = tk.Label(root, text="Score: {:d}".format(SCORE))
score_text.place(x=700, y=10)

level_text = tk.Label(root, text="Level: {:d}".format(LEVEL))
level_text.place(x=500, y=10)

time_text = tk.Label(root, text="Expected selection time: NaN sec")
time_text.place(x=0, y=10)

elapsed_text = tk.Label(root, text="Actual selection time: NaN sec")
elapsed_text.place(x=250, y=10)

root.mainloop()